import {
  Server,
  ServerRequest,
} from "https://deno.land/std@v0.42.0/http/server.ts";

// Types
import {
  Routes,
  ServerConfig,
  Middleware,
  Method,
  Request,
} from "./types/http.ts";

const { listen } = Deno;

export default class Fabric {
  private readonly server: Server;
  private routes: Routes = {};
  private options: ServerConfig;
  private middlewares: Middleware[] = [];

  constructor(options: ServerConfig) {
    this.options = options;

    this.server = this.serve();

    this.handleRequests();
  }

  private serve(): Server {
    let listener: any;
    let _runningOnLog: string = "Fabric server is available at: ";

    // Use the host > port if provided.
    if (this.options.host) {
      const [hostname, port] = this.options.host.split(":");

      listener = listen({ hostname, port: Number(port) });
      _runningOnLog += this.options.host;
    } else {
      listener = listen({ port: this.options.port });
      _runningOnLog += `http://localhost:${this.options.port}`;
    }

    // Log server start
    console.log(_runningOnLog);

    // Log any addtional strings that the user passes
    if (this.options.launchOptions?.consoleLog) {
      if (typeof this.options.launchOptions.consoleLog === "string") {
        this.options.launchOptions.consoleLog = [
          this.options.launchOptions.consoleLog,
        ];
      }

      for (const _option of this.options.launchOptions.consoleLog) {
        console.log(`${_option}\n`);
      }
    }

    return new Server(listener);
  }

  private async parseBody(req: ServerRequest): Promise<{}> {
    const buffer: Uint8Array = new Uint8Array(req.contentLength || 0);
    const lengthRead: number = await req.body.read(buffer) || 0;
    const rawBody: string | object = new TextDecoder().decode(
      buffer.subarray(0, lengthRead),
    );
    let body: {} = {};

    try {
      body = JSON.parse(rawBody.toString());
    } catch (error) {
      if (rawBody.toString().includes(`name="`)) {
        body = (rawBody.toString().match(
          /name="(.*?)"(\s|\n|\r)*(.*)(\s|\n|\r)*---/gm,
        ) || [])
          .reduce((fields: object, field: string): object => {
            if (!/name="(.*?)"/.exec(field)?.[1]) return fields;
            return {
              ...fields,
              [/name="(.*?)"/.exec(field)?.[1] || ""]: field.match(
                /(.*?)(?=(\s|\n|\r)*---)/,
              )?.[0],
            };
          }, {});
      }
    }
    return body;
  }

  private parseQuery(req: ServerRequest): {} {
    const queryURL = req.url.replace(/(.*)\?/, "");
    const queryString = queryURL.split("&");

    return queryString.reduce((queries: object, query: string): object => {
      if (!query) {
        return queries;
      }
      return {
        ...queries,
        [query.split("=")?.[0]]: query.split("=")?.[1],
      };
    }, {}) || {};
  }

  private parseCookies(req: ServerRequest): {} {
    const rawCookieString = req.headers.get("cookie");
    return rawCookieString &&
        rawCookieString.split(";").reduce(
          (cookies: any, cookie: string): any => {
            return {
              ...cookies,
              [cookie.split("=")[0].trimLeft()]: cookie.split("=")[1],
            };
          },
          {},
        ) || {};
  }
  private async handleRequests() {
    for await (const req of this.server) {
      if (this.options.ignoreTrailingSlash) {
        req.url = req.url.replace(/\/$/, "") + "/";
      }

      const routingPath = req.url.replace(/(\?(.*))|(\#(.*))/, "");

      if (!this.routes[req.method + routingPath]) {
        req.respond({ status: 404, body: "No registered route found." });
      } else {
        const userFriendlyRequest: Request = {
          raw: req,
          url: req.url,
          headers: req.headers,
          method: (req.method as Method),
          query: this.parseQuery(req),
          body: await this.parseBody(req),
          cookies: this.parseCookies(req),
          _responseHeaders: new Headers(),
          _responseStatusCode: 200,
          setStatusCode(statusCode: number) {
            this._responseStatusCode = statusCode;
          },
          setHeader(name: string, value: string) {
            this._responseHeaders.append(name, value);
          },
          setCookie(name: string, value: string) {
            this._responseHeaders.append("Set-Cookie", `${name}=${value}`);
          },
        };

        const respondValue = this.middlewares.reduce(
          (respondValue: string, middleware: Middleware): string => {
            return middleware(req, respondValue);
          },
          await this.routes[req.method + routingPath](userFriendlyRequest),
        );

        req.respond(
          {
            status: userFriendlyRequest._responseStatusCode,
            headers: userFriendlyRequest._responseHeaders,
            body: respondValue,
          },
        );
      }
    }
  }

  public register(middleware: Middleware) {
    this.middlewares.push(middleware);
  }

  public route(path: string, method: Method, callback: (req: Request) => any) {
    if (!path.startsWith("/")) {
      console.warn("Routes must start with a slash");
      return;
    }

    if (this.options.ignoreTrailingSlash) path = path.replace(/\/$/, "") + "/";
    this.routes[method + path] = callback;
    return this;
  }

  // cruds
  public get(path: string, callback: (req: Request) => any) {
    this.route(path, Method.GET, callback);
  }

  public post(path: string, callback: (req: Request) => any) {
    this.route(path, Method.POST, callback);
  }

  public put(path: string, callback: (req: Request) => any) {
    this.route(path, Method.PUT, callback);
  }

  public patch(path: string, callback: (req: Request) => any) {
    this.route(path, Method.PATCH, callback);
  }

  public delete(path: string, callback: (req: Request) => any) {
    this.route(path, Method.DELETE, callback);
  }
}
