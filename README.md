# Fabric

## Setup

[Install Deno](https://deno.land/#installation)

That's it. Seriously. No more tooling.

## Testing

Test files must follow this format: `<name>_test.ts`.

To run tests locally: `deno test --allow-net`.

### Running Fabric

If you want to play around with the source code you can create a `main.ts` file on the root.

```typescript
/// in main.ts
import Fabric from "./fabric.ts";

// Types
import { Request } from './types/http.ts'

const app = new Fabric({
  port: 8080,
  launchOptions: {
    consoleLog: "Start up console log!",
  },
});

app.get("/", (req: Request) => {
  return "Hello, World!!";
});

```

In your terminal: `deno run --allow-net main.ts`

### Bundle

`deno bundle fabric.ts dist/fabric.bundle.js`

You can now change your import path to use the `fabric.bundle.js` file!

<a target="_blank" href="https://icons8.com/icons/set/patch">Patch icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
