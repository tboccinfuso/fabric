import {
  ServerRequest,
} from "https://deno.land/std@v0.42.0/http/server.ts";

export enum Method {
  GET = "GET",
  POST = "POST",
  PUT = "PUT",
  DELETE = "DELETE",
  PATCH = "PATCH"
}

export type Request = {
  raw: ServerRequest;
  url: string;
  method: Method;
  headers: Headers;
  query: {};
  body: {};
  cookies: {};
  _responseHeaders: Headers;
  _responseStatusCode: number;
  setStatusCode(statusCode: number): void;
  setHeader(name: string, value: string): void;
  setCookie(name: string, value: string): void;
};

export type Routes = {
  [key: string]: (req: Request) => any;
};

export type ServerConfig = {
  host?: string;
  port: number;
  ignoreTrailingSlash?: boolean;
  launchOptions?: {
    consoleLog?: string | string[];
  };
};

export type Middleware = (req: ServerRequest, respondValue: string) => string;

/** Options for creating an HTTP server. */
export type HTTPOptions = Omit<Deno.ListenOptions, "transport">;
